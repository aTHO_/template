<?php
/**
 * Set all global variable
 * 
 * Global variable template
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

/*-----| All path |-----*/
    /*-- For url --*/                       $url_dir_path   =       "";
    /*-- path from / --*/                   $dir_path       =       "/var/www/to/project/";
    /*-- URL for IMG --*/                   $url_IMG_path   =       $url_dir_path .  "static/IMG/";
    /*-- URL for CSS --*/                   $url_CSS_path   =       $url_dir_path .  "static/CSS/";
    /*-- URL for JS --*/                    $url_JS_path    =       $url_dir_path .  "static/JS/";
    /*-- URL for font --*/                  $url_JS_path    =       $url_dir_path .  "static/font/";


/*-----| DB access |-----*/
    class DB_ACCESS{
        public static                       $DB_URL         =       "127.0.0.1";
        public static                       $DB_USERNAME    =       "username";
        public static                       $DB_PASSWD      =       "************";
    }
?>