#!/usr/bin/bash

#*******************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Bash arg Template       ****#
#*----- Contact :       https://athomisos.fr                                            ****#
#*----- Description :   Install.sh template flask use case                              ****#
#*******************************************************************************************#

PROGNAME=$(basename $0)
RELEASE="Revision 1.0"
AUTHOR="(c) 2021 Aubertin Emmanuel / https://athomisos.fr"
DEBUG=0

# Functions plugin usage
print_release() {
    echo "$RELEASE $AUTHOR"
}

print_usage() {
        echo ""
        echo "$PROGNAME"
        echo ""
        echo "Usage: $PROGNAME | [-h | --help] | [-v | --version] | [-d | --debug]"
        echo ""
        echo "          -h  Aide"
        echo "          -v  Version"
        echo "          -d  Debug"
        echo ""
        echo "Ce script est compatible avec un appel via crontab."
        if [ "$(dirname ${0})" == "." ]; then # For dynamic path execution (ex : ./)
            echo "echo \"0/5 * * * * root $(pwd -P)/$(basename ${0})\" > /etc/cron.d/Watcher-Soft"
        else
            echo "echo \"0/5 * * * * root ${0}\" > /etc/cron.d/Watcher-Soft"
        fi
}

print_help() {
        print_release $PROGNAME $RELEASE
        echo ""
        print_usage
        echo ""
        echo ""
                exit 0
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)
            print_help
            exit 
            ;;
        -v | --version)
                print_release
                exit 
                ;;
        -d | --debug)
                DEBUG=1
                ;;                                  
        *)  echo "Unkown argument: $1"
            print_usage
            ;;
        esac
shift
done

if [ $UID -ne 0 ]; then
    echo -e "\e[1;31mError :\e[22m To install <INSERT_NAME> you need root privileges\e[0m"
    exit 1
fi

#Install python3 and lib
echo -e "\e[32m--------| \e[1;32mINSTALATION OF DEPENDENCIES\e[32m |--------\e[0m"
apt install -y python3-pip sqlite3 git

echo -e "\e[32m--------| \e[1;32mPYTHON3 LIB\e[32m |--------\e[0m"
pip3 install sqlitepy python-dotenv Flask Flask-Assets Flask-Cors Flask-Mail

## GIT CLONE
echo -e "\e[32m--------| \e[1;32mGIT CLONE\e[32m |--------\e[0m"
git clone https://www.yourgit.com/YOUR_REPOSITORY.git