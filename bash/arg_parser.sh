#!/usr/bin/bash

#*******************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Bash arg Template       ****#
#*----- Contact :       https://athomisos.fr                                            ****#
#*----- Description :   Simple arg program                                              ****#
#*******************************************************************************************#

PROGNAME=$(basename $0)
RELEASE="Revision 1.0"
AUTHOR="(c) 2021 Aubertin Emmanuel / Twitter : @BlenderAubertin"
DEBUG=0

# Functions plugin usage
print_release() {
    echo "$RELEASE $AUTHOR"
}

print_usage() {
        echo ""
        echo "$PROGNAME"
        echo ""
        echo "Usage: $PROGNAME | [-h | --help] | [-v | --version] | [-d | --debug]"
        echo ""
        echo "          -h  Aide"
        echo "          -v  Version"
        echo "          -d  Debug"
        echo ""
        echo "Ce script est compatible avec un appel via crontab."
        if [ "$(dirname ${0})" == "." ]; then # For dynamic path execution (ex : ./)
            echo "echo \"0/5 * * * * root $(pwd -P)/$(basename ${0})\" > /etc/cron.d/Watcher-Soft"
        else
            echo "echo \"0/5 * * * * root ${0}\" > /etc/cron.d/Watcher-Soft"
        fi
}

print_help() {
        print_release $PROGNAME $RELEASE
        echo ""
        print_usage
        echo ""
        echo ""
                exit 0
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)
            print_help
            exit 
            ;;
        -v | --version)
                print_release
                exit 
                ;;
        -d | --debug)
                DEBUG=1
                ;;                                  
        *)  echo "Unkown argument: $1"
            print_usage
            ;;
        esac
shift
done