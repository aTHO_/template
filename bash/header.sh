#!/usr/bin/bash

#*******************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Bash Template           ****#
#*----- Contact :       https://athomisos.fr                                            ****#
#*----- Description :   Simple header                                                   ****#
#*******************************************************************************************#

PROGNAME=$(basename $0)
RELEASE="Revision 1.0"
AUTHOR="(c) 2021 Aubertin Emmanuel / Contact : https://athomisos.fr"
DEBUG=0