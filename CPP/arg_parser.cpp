/***************************************************************************************/
/*----- Auteur :        Aubertin Emmanuel               |  For: arg parser in cpp   ****/
/*----- Description :   Arg parser for cpp                                          ****/
/*----- Contact :       https://athomisos.fr                                        ****/
/***************************************************************************************/

#include <iostream>
#include <string.h>
#include <fstream>
#include <stdlib.h>

using namespace std; // Optional namespace

std::string PROGNAME="Numbers round solver";
std::string FILE_NAME= __FILE__;
std::string RELEASE="Revision 1.0 | Last update 29 december 2021";
std::string AUTHOR="\033[1mAubertin Emmanuel\033[0m";
std::string COPYRIGHT="(c) 2021 "+ AUTHOR + " from https://athomisos.fr";
bool VERBOSE = false;


void print_release() {
    std::cout << RELEASE << COPYRIGHT << endl;
}


void print_usage() {
        std::cout << endl 
        << PROGNAME << " by " << AUTHOR <<endl 
        << "\033[1mUsage: \033[0m"<< FILE_NAME <<" | [-h | --help] | [-v | --version] | [-V | --verbose] | filename" << endl
        << "          -h        help" << endl
        << "          -v        Version" << endl
        << "          -v        Verbose" << endl
        << "          filename  'number.txt' by default" << endl;
}


void print_help() {
        print_release();
        std::cout << endl;
        print_usage();
        std::cout << endl << endl;
        exit(0);
}


void failure(std::string message){
    std::cerr << "❌ \033[1;31m Error :\033[0m " << message << " ❌" << endl;
    exit(-1);
}

int main(int argc,char** argv){
    if(argc < 1) // number of arg minimum 
		failure("One argument required. \n\t-h for help");

    for(int i = 1; i < argc; i++){
        if (!strcmp(argv[i] , "-h") || !strcmp(argv[i] , "--help")){
            print_usage();
            exit(0);
        } else if (!strcmp(argv[i] , "-v") || !strcmp(argv[i] , "--version")){
            print_release();
            exit(0);
         } else if (!strcmp(argv[i] , "-V") || !strcmp(argv[i] , "--verbose")){
            std::cout << "✌  |Welcome in \033[1mVERBOSE\033[0m mode| ✌ " << endl;
            VERBOSE = true;
        } else { // ALL OTHER ARGUMENT
            print_usage();
            std::string err = "Unknow argument : " + *argv[i];
            failure(err);
        }
    }

    return 0;
    }