#***************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Julia header        ****#
#*----- Description :   Simple header template for julia                            ****#
#*----- Contact :       https://athomisos.fr                                        ****#
#***************************************************************************************#

#-------------------------------------------------
# Problem description :
#-------------------------------------------------
#  Variables :
#    a >= 0
#    b >= 0
#  Objective Function :
#    f = max 1000a + 1200b
#  Constraint :
#    2a <= 1000
#    4a + 3b <= 3000
#    2a + 3b <= 2401