#***************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Julia inti Pkg      ****#
#*----- Description :   Install dependencies for jl                                 ****#
#*----- Contact :       https://athomisos.fr                                        ****#
#***************************************************************************************#

import Pkg

Pkg.add("JuMP")
Pkg.add("Cbc")
Pkg.add("GLPK")

using JuMP
using Cbc
using GLPK