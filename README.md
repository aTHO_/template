# The templates of aTHO

In this repository, you will find all my templates. Feel free to use them as you wish.

## What's inside?

All the header files, it's my header signature in this language.

### Bash:

[arg_parser.sh](bash/arg_parser.sh) => Basic argument parser with -h, -v and -d.

[install.sh](bash/install.sh) => Simple install script. In this case it just installs the Flask application dependencies.

### CPP :

[arg_parser.cpp](CPP/arg_parser.cpp) => Argument parser with --help, --version and --verbose.

[Makefile](CPP/Makefile) => Simple Makefile that compiles and executes without arguments.

[makefile_with_txt_input](CPP/makefile_with_txt_input) => Basic makefile that compiles and executes with an input1.txt and input2.txt as argument.

### GitLab:

[GitLabCI_CPP.yml](GitLab/CI/GitLabCI_CPP.yml) => GitLab CI for your cpp project. Should be renamed to `.gitlab-ci.yml` and placed in the root of your project.

### Julia:

You must know [Linear programming](https://en.wikipedia.org/wiki/Linear_programming) from Wath.

[dual.jl](julia/dual.jl) => Basic solver that gives you the soluiton and the dual.

[init.jl](julia/init.jl) => Install the dependencies for the linear programming solver julia. 

[simple-problem.jl](julia/simple-problem.jl) => Simple solver that gives you the solution of your linear programming problem.

### PHP :

[config.php](php/config.php) => Just a basic configuration file for declaring your global variables.

### Python:

Empty for now. Soon available :)

Translated with www.DeepL.com/Translator (free version)